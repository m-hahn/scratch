
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import random
l1 = []
l1.append("ne Stem")
l1.append("Stem pas")
l1.append("ne Stem pas")
l1.append("Stem")

l2 = []
l2.append("ne Stem pas")
l2.append("Stem")
l2.append("ne Stem pas")
l2.append("Stem")


print(l1)
print(l2)

from estimateTradeoffInSample import estimateTradeoffInSample

def numerify(word, dic):
    if word not in dic:
        dic[word] = len(dic)
    return dic[word]


def languageToProcess(li):
  d = {"EOS" : 2, "SOS" : 1, "PAD" : 0}
  inp = []
  for _ in range(100):
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(0)
      inp.append(1)
      inp += [numerify(q, d) for q in random.choice(li).split(" ")]
      inp.append(2)
  return inp

l1_surp = (estimateTradeoffInSample(languageToProcess(l1), args = {"cutoff" : 7}))[1]
l2_surp = (estimateTradeoffInSample(languageToProcess(l2), args = {"cutoff" : 7}))[1]



from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import math

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(2, 2))

ax.set_xlabel("Memory")
ax.set_ylabel("Surprisal")

def compute_tradeoff(surps):
    memory = [0]
    surprisal = [surps[0]]
    for i in range(1, len(surps)):
        mi = surps[i-1] - surps[i]
        memory.append(memory[i-1] + i*mi)
        surprisal.append(surps[i])
    memory.append(2)
    surprisal.append(surprisal[-1])
    return memory, surprisal

l1_mem, l1_surp = compute_tradeoff(l1_surp)
l2_mem, l2_surp = compute_tradeoff(l2_surp)

ax.plot(l1_mem, l1_surp, color="red")
ax.plot(l2_mem, l2_surp, color="blue")


plt.savefig(f"figures/{__file__[:-3]}.pdf")
plt.show()

